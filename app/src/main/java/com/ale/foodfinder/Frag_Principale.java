package com.ale.foodfinder;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ale on 07/06/16.
 */
public class Frag_Principale extends Fragment {
    private static final String TAG="Frag_Principale";
    public CallbackManager callbackManager;
    public ShareDialog shareDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_principale, container, false);
        setHasOptionsMenu(true);
        return(v);
    }



    @Override
    public void onViewCreated (View view, Bundle savedIstanceState) {
        //Ricette
        Button ricette=(Button) view.findViewById(R.id.bottone_ricette);
        ricette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getActivity();
                //Posizionamento Frag_Lista
                android.app.FragmentManager FM = getFragmentManager();
                FragmentTransaction FT = FM.beginTransaction();
                Frag_Lista FL = new Frag_Lista();
                FT.replace(R.id.posto_per_fragment,FL,"Frag_Lista");
                FT.addToBackStack("Frag_Lista");
                FT.commit();
                //FM.executePendingTransactions();

                //Aggiorno pulsante menù
                getActivity().invalidateOptionsMenu();
            }
        });
        //Scegli per meAlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        Button scegli_per_me=(Button) view.findViewById(R.id.bottone_scegli_per_me);
        scegli_per_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context=getActivity();
                finestra_dialogo_scegli_per_me();
            }
        });
        /*ShareButton shareButton=(ShareButton)getActivity().findViewById(R.id.share_button);
        shareButton.setVisibility(View.GONE);*/
    }



    public void finestra_dialogo_scegli_per_me() {
        final View dialog_scegli=View.inflate(getActivity(),R.layout.finestra_scegli_per_me,null);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Parametri ricetta");
        builder.setView(dialog_scegli);
        builder.setCancelable(true);

        //Azione filtra
        builder.setPositiveButton("Scegli per me!",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                int ok=0;
                String[] par=new String[2];
                TextView t1=(TextView)  dialog_scegli.findViewById(R.id.scegli_ingrediente_textview);
                TextView t2=(TextView) dialog_scegli.findViewById(R.id.scegli_attributo_textview);
                par[0]=t1.getText().toString();
                par[1]=t2.getText().toString();
                if(par[0].equals("Scegli un ingrediente")) {
                    Toast.makeText(getActivity(),"Devi scegliere un ingrediente per continuare",Toast.LENGTH_LONG).show();
                    ok=1;
                }
                if(par[1].equals("Scegli un attributo")) {
                    Toast.makeText(getActivity(),"Devi scegliere un attributo per continuare",Toast.LENGTH_LONG).show();
                    ok=1;
                }
                dialog.cancel();
                if(ok==0) {
                    String[]s1=new String[1];
                    String[]s2=new String[1];
                    s1[0]=par[0];
                    s2[0]=par[1];
                    Cursor cursor=((MainActivity)getActivity()).database.query_filtri(new String[1],s1,s2);
                    if(cursor!=null) {
                        if(cursor.moveToFirst()) {
                            int indice=gestione_random(cursor);
                            Log.d(TAG,"Visualizzerò la ricetta all'indice  "+indice);
                            raccogli_info_e_mostra_dettagli(cursor,indice);
                        }
                        else Toast.makeText(getActivity(),"Non è stata trovata nessuna ricetta con\nl'ingrediente "+par[0]+"\ne con l'attributo "+par[1],Toast.LENGTH_LONG).show();
                    }
                    else Toast.makeText(getActivity(),"Non è stata trovata nessuna ricetta con\nl'ingrediente "+par[0]+"\ne con l' attributo "+par[1],Toast.LENGTH_LONG).show();
                }
            }
        });

        //Azione cancella
        builder.setNegativeButton("Chiudi",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });

        //Genera le due liste
        genera_liste_dialog_scegli(dialog_scegli);

        builder.show();
    }
    public void genera_liste_dialog_scegli(View v) {
        ArrayList<String> ing_disponibili=ottieni_disponibili("Ing");
        ArrayList<String> attr_disponibili=ottieni_disponibili("Attr");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, ing_disponibili);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, attr_disponibili);
        final ListView l1=(ListView) v.findViewById(R.id.scegli_listview_ingrediente);
        final ListView l2=(ListView) v.findViewById(R.id.scegli_listview_attributo);
        final TextView t1=(TextView) v.findViewById(R.id.scegli_ingrediente_textview);
        final TextView t2=(TextView) v.findViewById(R.id.scegli_attributo_textview);
        l1.setAdapter(adapter1);
        l2.setAdapter(adapter2);

        l1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = parent.getItemAtPosition(position);
                t1.setText(o.toString());
                l1.setVisibility(View.GONE);
            }
        });
        l2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = parent.getItemAtPosition(position);
                t2.setText(o.toString());
                l2.setVisibility(View.GONE);
            }
        });


    }
    public ArrayList<String> ottieni_disponibili(String s) {
        ArrayList<String> ritorno=new ArrayList<String>();
        if (s.equals("Ing")) {
            DBhelper dBhelper=new DBhelper(getActivity());
            Cursor cursor=dBhelper.ottieni_tutto();
            if(cursor!=null) {
                if(cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        String ing_lunghi=cursor.getString(cursor.getColumnIndex(DBhelper.COL_INGREDIENTI));
                        Ricetta ricetta=new Ricetta("","","","","","","");
                        String ing_e_quantita[][]=ricetta.ottieni_ingredienti_e_quantita(ing_lunghi);
                        for(int i=0;i<ing_e_quantita.length;i++) {
                            if (!(ritorno.contains(ing_e_quantita[i][0]))) {
                                ritorno.add(ing_e_quantita[i][0]);
                            }
                        }
                        cursor.moveToNext();
                    }
                }
            }
            cursor.close();
        }
        else {
            DBhelper dBhelper=new DBhelper(getActivity());
            Cursor cursor=dBhelper.ottieni_tutto();
            if(cursor!=null) {
                if(cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        String attr_lunghi=cursor.getString(cursor.getColumnIndex(DBhelper.COL_ATTRIBUTI));
                        Ricetta ricetta=new Ricetta("","","","","","","");
                        String[] attr=ricetta.ottieni_attributi(attr_lunghi);
                        for(int i=0;i<attr.length;i++) {
                            if(!(ritorno.contains(attr[i]))) {
                                ritorno.add(attr[i]);
                            }
                        }
                        cursor.moveToNext();
                    }
                }
            }
            cursor.close();
        }
        return  ritorno;
    }
    public int gestione_random(Cursor cursor) {
        Random random=new Random();
        int ritorno,min=0,max=cursor.getCount();
        ritorno=random.nextInt(max-min)+min;
        return ritorno;
    }
    public void raccogli_info_e_mostra_dettagli(Cursor cursor,int i) {
        ArrayList<String> info=new ArrayList<String>();
        int j;
        //sposto il cursor all'elemento che voglio
        for(j=0;j<i;j++) {
            cursor.moveToNext();
        }
        String nome=cursor.getString(cursor.getColumnIndex(DBhelper.COL_NOME));
        String tipo=cursor.getString(cursor.getColumnIndex(DBhelper.COL_TIPO));
        String difficolta=cursor.getString(cursor.getColumnIndex(DBhelper.COL_DIFFICOLTA));
        String attributi=cursor.getString(cursor.getColumnIndex(DBhelper.COL_ATTRIBUTI));
        String ingredienti=cursor.getString(cursor.getColumnIndex(DBhelper.COL_INGREDIENTI));
        String descrizione=cursor.getString(cursor.getColumnIndex(DBhelper.COL_DESCRIZIONE));
        String immagine=cursor.getString(cursor.getColumnIndex(DBhelper.COL_IMMAGINE));

        info.add(nome);
        info.add(tipo);
        info.add(difficolta);
        info.add(attributi);
        info.add(ingredienti);
        info.add(descrizione);
        info.add(immagine);

        //Bundle
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("key",info);

        //Fragment
        android.app.FragmentManager manager = ((MainActivity) getActivity()).getFragmentManager();
        FragmentTransaction FT = manager.beginTransaction();
        Frag_Dettagli FL = new Frag_Dettagli();
        FL.setArguments(bundle);
        FT.replace(R.id.posto_per_fragment,FL,"Frag_Dettagli");
        FT.addToBackStack("Frag_Dettagli");
        FT.commit();

        //Aggiorno menù
        ((MainActivity)getActivity()).invalidateOptionsMenu();
    }
}
