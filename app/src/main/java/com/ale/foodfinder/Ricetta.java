package com.ale.foodfinder;

import java.util.ArrayList;

/**
 * Created by ale on 08/06/16.
 */
public class Ricetta {
    public String nome;
    public String tipo;
    public String immagine;
    public String attributi;
    public String ingredienti;
    public String difficolta;
    public String descrizione;

    public Ricetta( String nome2,
                    String tipo2,
                    String immagine2,
                    String attributi2,
                    String ingredienti2,
                    String difficolta2,
                    String descrizione2) {
        nome=nome2;
        tipo=tipo2;
        immagine=immagine2;
        attributi=attributi2;
        ingredienti=ingredienti2;
        difficolta=difficolta2;
        descrizione=descrizione2;
    }
    //La matrice che ritorna è compsosta così:
    // M[0][0] ingrediente1 M[0][1] quantità1
    // M[1][0] ingrediente2 M[1][1] quantità2
    public String[][] ottieni_ingredienti_e_quantita(String s) {
        String[] split1=s.split("___");
        String[][] secondSplit= secondSplit = new String[split1.length][];

        for(int i=0; i<split1.length; i++){
            secondSplit[i] = split1[i].split(":");
        }
        return (secondSplit);
    }
    public String[] ottieni_attributi(String s) {
        String[] split1=s.split("___");
        return (split1);
    }

}
