package com.ale.foodfinder;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.widget.ShareButton;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by ale on 13/06/16.
 */
public class Frag_Dettagli extends Fragment{
    private static final String TAG="Frag_Dettagli";
    public String[] dettagli_da_condividere=new String[7];
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_dettagli, container, false);
        return(v);
    }
    @Override
    public void onViewCreated (View view, Bundle savedIstanceState) {
        Bundle bundle = this.getArguments();
        final ArrayList<String> info=bundle.getStringArrayList("key");
        TextView t_nome=(TextView) getView().findViewById(R.id.nome_ricetta);
        TextView t_tipo=(TextView) getView().findViewById(R.id.tipo);
        TextView t_difficolta=(TextView) getView().findViewById(R.id.difficolta);
        TextView t_attributi=(TextView) getView().findViewById(R.id.attributi);
        TextView t_ingredienti=(TextView) getView().findViewById(R.id.ingredienti);
        TextView t_descrizione=(TextView) getView().findViewById(R.id.descrizione);
        ImageView t_immagine=(ImageView) getView().findViewById(R.id.immagine_ricetta);



        t_nome.setText(info.get(0));
        t_tipo.setText(info.get(1));
        t_difficolta.setText(info.get(2));
        t_descrizione.setText(info.get(5));

        //Ricetta vuota per poter poter usare le sue funzioni
        Ricetta ric=new Ricetta("","","","","","","");
        String[][] ritorno=ric.ottieni_ingredienti_e_quantita(info.get(4));
        String ing_set="";
        int i,j;
        for(i=0;i<ritorno.length;i++) {
            ing_set=ing_set+ritorno[i][0]+
                    ":"+
                    ritorno[i][1]+
                    "\n";
        }
        t_ingredienti.setText(ing_set);
        String[] ritorno2=ric.ottieni_attributi(info.get(3));
        String attr_set="";
        for(i=0;i<ritorno2.length;i++) {
            attr_set=attr_set+ritorno2[i]+" ";
        }
        t_attributi.setText(attr_set);

        String srcImmagine=new String();
        if(info.get(6).contains("standard")) {
            String tmp[]=info.get(6).split(":");
            srcImmagine=tmp[1];
            int drawableID = getActivity().getResources().getIdentifier(srcImmagine,
                "drawable",
                getActivity().getPackageName());
            t_immagine.setImageResource(drawableID);

            //pulsante cancella non si vede
        }
        else {
            srcImmagine=info.get(6);
            food_adapter f_vuoto=new food_adapter(getActivity(),new ArrayList<Food_Card>());
            Bitmap bitmap=f_vuoto.StringToBitMap(srcImmagine);
            t_immagine.setImageBitmap(bitmap);

            //rendo visibile il pulsante cancella
            FloatingActionButton fab_cancella  = (FloatingActionButton)getView().findViewById(R.id.cancella);
            fab_cancella.setVisibility(View.VISIBLE);
            fab_cancella.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                    builder.setTitle("Vuoi elimare la tua ricetta?");
                    builder.setCancelable(false);

                    builder.setPositiveButton("Cancella ricetta",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //nome,tipo,immagine,attributi,ingredienti,difficolta,descrizione
                            Ricetta ricetta=new Ricetta(info.get(0),info.get(1),info.get(6),info.get(3),info.get(4),info.get(2),info.get(5));

                            DBhelper db=((MainActivity)getActivity()).getDatabase();
                            if(db.cancella_ricetta(ricetta)) {
                                Log.d(TAG,"Cancellazione effettuata");
                            }
                            else Log.d(TAG,"Errore cancellazione");
                            android.app.FragmentManager FM = getFragmentManager();
                            FragmentTransaction FT = FM.beginTransaction();
                            Frag_Lista FL = new Frag_Lista();
                            FT.replace(R.id.posto_per_fragment,FL,"Frag_Lista");
                            FT.addToBackStack("Frag_Lista");
                            FT.commit();

                            getActivity().invalidateOptionsMenu();
                        }
                    });
                    builder.setNegativeButton("Chiudi",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                }
            });
        }

        //Riempio variabile da condividere
        for(int kkk=0;kkk<dettagli_da_condividere.length;kkk++) {
            try {
                dettagli_da_condividere[kkk]=info.get(kkk);
            }
            catch (Exception e) {
                Log.e(TAG,e.toString());
            }
        }
        /*ShareButton shareButton=(ShareButton)getActivity().findViewById(R.id.share_button);
        shareButton.setVisibility(View.VISIBLE);*/
    }
    public String[] ottieni_per_condividere() { return dettagli_da_condividere; }


}
