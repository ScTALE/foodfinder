package com.ale.foodfinder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by ale on 08/06/16.
 */
public class DBhelper extends SQLiteOpenHelper {
    private final static String TAG="DBhelper";
    public Context c;
    private static final String DB_NAME = "grades.db";
    private static final int DB_VERSION = 1;
    public static final String TABELLA_RICETTE = "TabellaRicette";
    public static final String COL_ID="ColonnaId";
    public static final String COL_NOME="ColonnaNome";
    public static final String COL_TIPO = "ColonnaTipo";
    public static final String COL_IMMAGINE = "ColonnaImmagine";
    public static final String COL_ATTRIBUTI="ColonnaAttributi";
    public static final String COL_INGREDIENTI = "ColonnaIngredienti";
    public static final String COL_DIFFICOLTA="ColonnaDifficolta";
    public static final String COL_DESCRIZIONE="ColonnaDescrizione";

    public DBhelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
        c=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql_tab = "create table " + TABELLA_RICETTE + "( " +
                COL_ID + " integer primary key autoincrement, " +
                COL_NOME + " text not null, " +
                COL_TIPO + " text not null, " +
                COL_IMMAGINE + " text not null, " +
                COL_ATTRIBUTI + " text not null, " +
                COL_INGREDIENTI + " text not null, " +
                COL_DIFFICOLTA + " text not null, " +
                COL_DESCRIZIONE + " text not null " +
                ");";
        db.execSQL(sql_tab);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor ottieni_tutto() {
        return getWritableDatabase().query(TABELLA_RICETTE, null, null, null, null, null, null);
    }

    public long inserisci_ricetta(Ricetta ric) {
        ContentValues cv=new ContentValues();
        cv.put(COL_NOME,ric.nome);
        cv.put(COL_TIPO,ric.tipo);
        cv.put(COL_IMMAGINE,ric.immagine);
        cv.put(COL_ATTRIBUTI,ric.attributi);
        cv.put(COL_INGREDIENTI,ric.ingredienti);
        cv.put(COL_DIFFICOLTA,ric.difficolta);
        cv.put(COL_DESCRIZIONE,ric.descrizione);
        long code = getWritableDatabase().insert(TABELLA_RICETTE, null, cv);
        return code;
    }
    public Cursor query_filtri(String[] parametri,String[] p2,String[] p3) {
        ArrayList<String> da_mettere_in_whereArgs=new ArrayList<String>();
        String whereClause="";
        //count si comporta come i parametri del chmod
        int i,count=0;
        for(i=0;i<parametri.length;i++) {
            if(parametri[i]!=null) count=count+i+1;
        }
        switch (count) {
            case 0: {
                //Query senza parametri specifici, ritorno tutto
                whereClause=null;
               // whereArgs=null;
                Log.d(TAG,"Nessun parametro tipo e difficoltà");
                break;
            }
            case 1: {
                //Query su tipo
                whereClause=COL_TIPO+"=?";
                //whereArgs=new String[] {parametri[0]};
                da_mettere_in_whereArgs.add(parametri[0]);
                break;
            }
            case 2: {
                //Query su difficolta
                whereClause=COL_DIFFICOLTA+"=?";
               // whereArgs=new String[] {parametri[1]};
                da_mettere_in_whereArgs.add(parametri[1]);
                break;
            }
            case 3: {
                //Query su tipo e difficolta
                whereClause=COL_TIPO+"=? AND "+COL_DIFFICOLTA+"=?";
                da_mettere_in_whereArgs.add(parametri[0]);
                da_mettere_in_whereArgs.add(parametri[1]);
               // whereArgs=new String[] {parametri[0],parametri[1]};
                break;
            }
        }
        //parte ingredienti
        //k ricontrolla se ci sono ingredienti
        //count2fake controlla se la query è vuota o ci sono dei pezzi
        int j,k=0,count2fake=0;
        String[] temp=new String[3];
        if(p2.length!=100) {
            for (j = 0; j < p2.length; j++) {
                if (!(p2[j].matches(""))) {
                    temp[k] = p2[j];
                    k++;
                }
            }
            if (k > 0) {
                count2fake=1;
                if (count == 0) {
                    switch (k) {
                        case 1: {
                            whereClause = COL_INGREDIENTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp[0]+"%");
                            break;
                        }
                        case 2: {
                            whereClause = COL_INGREDIENTI + " like ? AND " + COL_INGREDIENTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp[1]+"%");
                            break;
                        }
                        case 3: {
                            whereClause = COL_INGREDIENTI + " like ? AND " + COL_INGREDIENTI + " like ? AND " + COL_INGREDIENTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp[1]+"%");
                            da_mettere_in_whereArgs.add("%"+temp[2]+"%");
                            break;
                        }
                    }
                } else {
                    switch (k) {
                        case 1: {
                            whereClause = whereClause + " AND " + COL_INGREDIENTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp[0]+"%");
                            break;
                        }
                        case 2: {
                            whereClause = whereClause + " AND " + COL_INGREDIENTI + " like ? AND " + COL_INGREDIENTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp[1]+"%");
                            break;
                        }
                        case 3: {
                            whereClause = whereClause + " AND " + COL_INGREDIENTI + " like ? AND " + COL_INGREDIENTI + " like ? AND " + COL_INGREDIENTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp[1]+"%");
                            da_mettere_in_whereArgs.add("%"+temp[2]+"%");
                            break;
                        }
                    }
                }
            }
            else Log.d(TAG,"Nessun ingrediente");
        }
        //parte attributi
        int a,b=0;
        String[]temp2=new String[3];
        if(p3.length!=100) {
            for (a = 0; a < p3.length; a++) {
                if (!(p3[a].matches(""))) {
                    temp2[b] = p3[a];
                    b++;
                }
            }
            if(b>0) {
                if((count2fake==0)&&(count==0)) {
                    switch (b) {
                        case 1: {
                            whereClause = COL_ATTRIBUTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp2[0]+"%");
                            break;
                        }
                        case 2: {
                            whereClause = COL_ATTRIBUTI + " like ? AND " + COL_ATTRIBUTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp2[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp2[1]+"%");
                            break;
                        }
                        case 3: {
                            whereClause = COL_ATTRIBUTI + " like ? AND " + COL_ATTRIBUTI + " like ? AND " + COL_ATTRIBUTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp2[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp2[1]+"%");
                            da_mettere_in_whereArgs.add("%"+temp2[2]+"%");
                            break;
                        }
                    }
                }
                else {
                    switch (b) {
                        case 1: {
                            whereClause = whereClause + " AND " + COL_ATTRIBUTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp2[0]+"%");
                            break;
                        }
                        case 2: {
                            whereClause = whereClause + " AND " + COL_ATTRIBUTI + " like ? AND " + COL_ATTRIBUTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp2[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp2[1]+"%");
                            break;
                        }
                        case 3: {
                            whereClause = whereClause + " AND " + COL_ATTRIBUTI + " like ? AND " + COL_ATTRIBUTI + " like ? AND " + COL_ATTRIBUTI + " like ?";
                            da_mettere_in_whereArgs.add("%"+temp2[0]+"%");
                            da_mettere_in_whereArgs.add("%"+temp2[1]+"%");
                            da_mettere_in_whereArgs.add("%"+temp2[2]+"%");
                            break;
                        }
                    }
                }
            }
            else Log.d(TAG,"Nessun attributo");
        }
         int fff=0;
        String[] whereArgs=new String[da_mettere_in_whereArgs.size()];
        for (String s: da_mettere_in_whereArgs) {
            whereArgs[fff]=s;
            fff++;
        }


        return  getWritableDatabase().query(TABELLA_RICETTE,null,whereClause,whereArgs,null,null,null);

    }

    public boolean cancella_ricetta(Ricetta ricetta) {
        try {
            String whereClause;
            String[] whereArgs = new String[50];
            SQLiteDatabase db=getWritableDatabase();
            //db.delete(TABELLA_RICETTE, "id = ?", new String[] { userName });
            whereClause=COL_NOME + " =? AND "+
                    COL_TIPO + " =? AND "+
                    COL_IMMAGINE + " =? AND "+
                    COL_INGREDIENTI + " =? AND "+
                    COL_ATTRIBUTI + " =? AND "+
                    COL_DIFFICOLTA + " =? AND "+
                    COL_DESCRIZIONE + " =?";
            whereArgs=new String[] {ricetta.nome,ricetta.tipo,ricetta.immagine,ricetta.ingredienti,ricetta.attributi,ricetta.difficolta,ricetta.descrizione};

            db.delete(TABELLA_RICETTE,whereClause,whereArgs);

            db.close();
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}



