package com.ale.foodfinder;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by ale on 08/06/16.
 */
public class Json_Standard extends MainActivity {
    private static final String TAG = "Json_Standard";
    public JSONObject main=new JSONObject();
    public Json_Standard(Context context){

        //jxx put()
        //ric.put(jx)
        //big.put(ric)
        JSONArray ric=new JSONArray();

        JSONObject d1=new JSONObject();
        JSONObject d2=new JSONObject();
        JSONObject d3=new JSONObject();

        JSONObject a1=new JSONObject();
        JSONObject a2=new JSONObject();
        JSONObject a3=new JSONObject();

        JSONObject p1=new JSONObject();
        JSONObject p2=new JSONObject();
        JSONObject p3=new JSONObject();

        JSONObject s1=new JSONObject();
        JSONObject s2=new JSONObject();
        JSONObject s3=new JSONObject();

        JSONObject c1=new JSONObject();
        JSONObject c2=new JSONObject();
        JSONObject c3=new JSONObject();

        //Ogni ingredienti viene separato da ___
        //La quantità di ogni ingrediente viene separata dall'ingrediente con :
        try {
            d1.put("Nome", "Torta al mars");
            d1.put("Tipo", "Dolce");
            d1.put("Immagine","standard:torta_al_mars");
            d1.put("Attributi","Sfiziosa___"+
                   "Semplice___"+
                    "Veloce");
            d1.put("Ingredienti","Riso soffiato:200g___" +
                   "Barrette Mars:300g___" +
                   "Burro:80g___" +
                   "Cioccolato al latte:100g");
             d1.put("Difficolta","Facile");
            d1.put("Descrizione","Per preparare la torta Mars iniziate foderando con della carta da forno una tortiera con stampo a cerniera da 24 cm di diametro . Tagliate i Mars a pezzetti   e metteteli a fondere in una casseruola con il burro   (potete anche utilizzare il forno a microonde);quando saranno ben fusi   aggiungeteli ai chicchi di riso soffiato (Rice Krispies)   e mescolate molto velocemente per amalgamare il tutto  . Attenzione, dovete svolgere questa operazione molto velocemente perché il caramello contenuto nei Mars si solidificherà  in fretta!Versate il composto nella tortiera   e livellatelo  , aiutandovi con un pezzetto di carta da forno: mettete un pezzetto di carta da forno sull'impasto e livellatelo con le mani.\n" +
                   "Mettete a rassodare la torta in frigorifero per un’ora. Sformate la torta e realizzate la decorazione: fondete al microonde il cioccolato al latte, mettetelo in una sac à poche e effettuate delle colate fitte di cioccolato  ." +
                   "");
            ric.put(d1);


            d2.put("Nome", "Torta della nonna");
            d2.put("Tipo", "Dolce");
            d2.put("Immagine","standard:torta_della_nonna");
            d2.put("Attributi","Articolata___"+
                   "Gustosa");
            d2.put("Ingredienti","Farina tipo 00:480g___"+
                   "Uova:5___"+
                   "Zucchero semolato:385g___"+
                   "Burro:200g___"+
                   "Scorza di limone:2___"+
                   "Latte intero:750g___"+
                   "Amido di mais:35g___"+
                   "Pinoli:25g___");
            d2.put("Difficolta","Media");
            d2.put("Descrizione","Per preparare la torta della nonna, iniziate dalla frolla: in un mixer ponete la farina e il burro freddo di frigo tagliato a piccoli pezzi , quindi azionate le lame per ottenere una sabbiatura: ci vorranno pochi istanti. Trasferite il composto sabbioso su una spianatoia, create una conca al centro e versate lo zucchero  , poi unite le uova sempre al centro   " +
                   "grattate anche la scorza di 1 limone (possibilmente non trattato)   e maneggiate brevemente gli ingredienti   giusto il tempo di compattare la frolla e non formare la maglia glutinica, quindi datele la forma di un panetto, appiattite leggermente e coprite con pellicola trasparente  ; riponete la frolla in frigo a rassodare per circa 30 minuti." +
                   "Nel frattempo realizzate la crema pasticcera: in un pentolino ponete il latte  , poi ricavate la scorza di 1 limone con un pelaverdure, facendo attenzione a non prelevare anche la parte bianca amarognola  ; unite la scorza al latte e fatelo scaldare a fuoco basso (dovrà sfiorare il bollore)  ." +
                   "In una ciotolina rompete le 3 uova intere e il tuorlo e versate lo zucchero  , quindi miscelate gli ingredienti e quando lo zucchero sarà assorbito aggiungete anche la farina e l'amido di mais setacciati in un colino  ; mescolate bene le polveri." +
                   "Intanto togliete la scorza di limone dal latte   e togliete il pentolino dal fuoco per stemperare leggermente con poco latte caldo le uova sbatutte con zucchero e le polveri  . Mescolate bene, poi rimettete il pentolino del latte sul fuoco e versate all'interno il composto con le uova  ." +
                   "Continuate a cuocere lentamente e mescolare sempre con una frusta la crema, perchè si addensi, ci vorranno circa 10-15 minuti. Una volta pronta, trasferitela in una pirofila bassa e ampia  " +
                   "e coprite subito con pellicola trasparente a contatto  . Lasciate intiepidire a temperatura ambiente. Nel frattempo riprendete la frolla, infarinate leggermente il piano di lavoro e stendetela fino a formare un disco dello spessore di 2-3 millimetri   (potete stendere la frolla tra due fogli di carta da forno per stenderla più facilmente). Imburrate e infarinate uno stampo da crostata del diametro di 26 cm; arrotolate la frolla sul mattarello e srotolatela sullo stampo  " +
                   "fate aderire bene il fondo e i bordi pressando con le dita   ed eliminate l'eccesso di pasta che userete per realizzare il guscio di copertura. Bucherellate il fondo con i rebbi di una forchetta e versate la crema pasticcera che si sarà raffreddata nel frattempo  . A questo punto stendete gli avanzi di pasta frolla e srotolate il disco ottenuto sulla teglia a coprire  ." +
                   "Passando con il mattarello sulla teglia eliminerete i bordi in eccesso (potete congelare la frolla che è avanzata e usarla in un'altra occasione!)  ; bucherellate la superficie   poi distribuite i pinoli  ." +
                   "La torta della nonna è pronta per essere infornata  : cuocetela in forno statico preriscaldato a 160° per 50 minuti nel ripiano più basso; passato questo tempo potete spostarla nel ripiano di mezzo del forno e alzare la temperatura a 180°, cuocendo per altri 10 minuti. Durante la cottura se vi accorgete che si scurisce troppo la superficie, potete coprire con un foglio di carta alluminio. Sfornate la torta della nonna   e fate raffreddare completamente, prima di spolverizzarla con zucchero a velo e servirla per una golosissima merenda o come dessert di fine pranzo  !" +
                   "");
            ric.put(d2);

            d3.put("Nome","Torta Sacher");
            d3.put("Tipo","Dolce");
            d3.put("Immagine","standard:torta_sacher");
            d3.put("Attributi","Articolata___"+
                    "Gustosa");
            d3.put("Ingredienti","Sale:2g___"+
                    "Cioccolato fondente:550g___"+
                    "Burro:175g___"+
                    "Zucchero semolato:260g___"+
                    "Miele:20g___"+
                    "Farina 00:180g___"+
                    "Uova:6___"+
                    "Marmellata albicocche:350g___"+
                    "Panna:250g___"+
                    "Glucosio:50g___");
            d3.put("Difficolta","Media");
            d3.put("Descrizione","Per realizzare la sacher iniziate a sciogliere il cioccolato fondente a bagnomaria o nel microonde , poi lasciatelo raffreddare, deve arrivare ad una temperatura di circa 32°," +
                    " verificate la temperatura con un termometro per alimenti, è importante che il cioccolato non sia troppo caldo altrimenti scioglierà il burro nell’impasto. " +
                    "Dividete i tuorli dagli albumi, ponete nella ciotola di una planetaria munita di frusta il burro a pezzetti a temperatura ambiente, unite 110 gr di zucchero semolato e il miele. " +
                    "Azionate la macchina fino ad ottenere un composto spumoso , poi unite a filo il cioccolato a 32° e i tuorli uno alla volta.Iniziate a montare gli albumi con un pizzico di sale ," +
                    " quando saranno bianchi, incorporate i restanti 150 gr di zucchero  poco alla volta, continuate a montare gli albumi a lucido ovvero devono rimanere un po’ cremosi. " +
                    "Aggiungete l’albume all’impasto  mescolate delicatamente con una spatola o la frusta dal basso verso l’alto per non smontare le uova.Aggiungete la farina setacciata  mescolate sempre delicatamente." +
                    "  Imburrate e foderate con carta da forno una tortiera di 24 cm e versate all’interno l’impasto , livellatelo con una spatola, non sbattete la tortiera altrimenti si smonterà il composto. " +
                    "Cuocete la torta in forno preriscaldato statico a 180° per 50/60 minuti (se ventilato a 160° per 40/50 minuti).A cottura ultimate sfornate la torta e fatela raffreddare su una gratella  " +
                    "poi dividetela in 3 dischi , setacciate la confettura in modo che sia liscia e senza grumi e farcite la torta  Spennellate anche tutta la superficie esterna con la confettura  lasciatela " +
                    "asciugare per almeno un ‘ora a temperatura ambiente. Intanto occupatevi della ricopertura: scaldate in un pentolino la panna fresca liquida, unite il glucosio  e mescolate per scioglierlo, " +
                    "quando il composto sfiorerà il bollore spegnete il fuoco e versatelo sul cioccolato fondente tritato . Mescolate fino ad ottenere una crema senza grumi, lasciate raffreddare la salsa, " +
                    "dovrà raggiungere i 32°, questa temperatura permette di ottenere la giusta densità necessaria a ricoprire la torta . Quando la salsa sarà fredda potrete ricoprire la torta: " +
                    "ponete la base su una gratella e disponete sotto un vassoio per raccogliere la ricopertura, fate colare la glassa al cioccolato iniziate dai lati  e poi nel centro,dovrete ricoprire interamente la torta . Per finire realizzate la scritta Sacher versando la glassa avanzata in un conetto di carta da forno o una sac-à-poche con una apertura molto stretta . La vostra sachertorte è pronta per essere gustata .");
            ric.put(d3);

            a1.put("Nome","Carpaccio di orata al basilico e limone");
            a1.put("Tipo","Antipasto");
            a1.put("Immagine","standard:carpaccio_di_orata_al_basilico_e_limone");
            a1.put("Attributi","Particolare___"+
                    "Vegetariana");
            a1.put("Ingredienti","Orata:500g___"+
                    "Limone:2___"+
                    "Insalata mista:1___"+
                    "Basilico:1___"+
                    "Olio extravergine di oliva:Qb___"+
                    "Pepe:Qb___");
            a1.put("Difficolta","Media");
            a1.put("Descrizione","Per la ricetta del carpaccio di orata al basilico e limone, sfilettate l’orata ed eliminate le spine. " +
                    "Tagliate i filetti in sottili fettine oblique, come si fa con il salmone affumicato. " +
                    "Lavorando sopra una ciotola per raccoglierne il succo, pelate a vivo i limoni e divideteli in spicchi." +
                    "Frullate 4 cucchiai di olio con il succo di limone, le foglie di un ciuffo di basilico tritate e un pizzico di sale (emulsione)." +
                    " Condite il carpaccio di orata con sale, pepe e l’emulsione (tenetene un po’ da parte) e lasciatelo marinare per 15′." +
                    "Servitelo con l’insalata, gli spicchi di limone, condito con qualche altra goccia di emulsione.");
            ric.put(a1);

            a2.put("Nome","Cheesecake con stracciatella alle erbe");
            a2.put("Tipo","Antipasto");
            a2.put("Immagine","standard:cheesecake_con_stracciatella_alle_erbe");
            a2.put("Attributi","Articolata___"+
                    "Saporita");
            a2.put("Ingredienti","Ricotta:40g___"+
                    "Stracciatella:300g___"+
                    "Farina 0:300g___"+
                    "Gelatina:15g___"+
                    "Lievito di birra:7g___"+
                    "Pomodori ciliegina:6___"+
                    "Rosmarino:Qb___"+
                    "Origano:Qb___"+
                    "Filetti di acciuga:Qb___"+
                    "Basilico:Qb___"+
                    "Olio extravergine di oliva:Qb___"+
                    "Sale:Qb___"+
                    "Pepe:Qb___");
            a2.put("Difficolta","Media");
            a2.put("Descrizione","Per la ricetta della cheesecake con stracciatella alle erbe, impastate la farina con g 170 di acqua, " +
                    "in cui avrete sciolto il lievito di birra e una presa di sale. Lasciate lievitare l’impasto fino a quando il suo volume non sarà raddoppiato. " +
                    "Stendete l’impasto, ungetelo di olio e cospargetelo con 2 cucchiai di trito aromatico, impastatelo ancora e lasciatelo riposare per altri 20′. " +
                    "Stendete di nuovo l’impasto e poi disponetelo su una placca unta, all’interno di un anello pennellato di olio (ø cm 23); " +
                    "spruzzate la pasta di acqua e lasciate lievitare ancora per 30′. Infornate infine a 180 °C per 20′ circa. " +
                    "Sfornate la focaccia, fatela raffreddare e tagliatela poi a metà orizzontalmente, così da ottenere due dischi. " +
                    "Frullate la stracciatella con 4-5 foglie di basilico, poi unitela alla ricotta, salate, pepate e amalgamate fino a ottenere una crema omogenea. " +
                    "Intanto sciogliete sul fuoco la gelatina, già ammollata e lievemente strizzata, e unitela alla ricotta mescolando con cura. " +
                    "Disponete il disco di base della focaccia su una placca senza bordi foderata di carta da forno, circondatelo con l’anello e distribuitevi la crema di ricotta. " +
                    "Livellate bene la superficie e lasciate raffreddare in frigorifero per almeno un paio di ore. " +
                    "Togliete dal frigo il cheesecake e, per sformarlo con agio, scaldate leggermente l’anello. " +
                    "L’ideale sarebbe farlo passandovi velocemente la fiamma di un cannello per caramellare. " +
                    "Dividete il secondo disco di focaccia in 10 spicchi. Distribuite sulla crema di ricotta, lungo il bordo, 10 mezzi pomodorini e coprite con gli spicchi di focaccia. " +
                    "Completate disponendo al centro del cheesecake un pomodorino tagliato a metà e qualche filetto di acciuga.");
            ric.put(a2);

            a3.put("Nome","Insalata di polpo e arancia");
            a3.put("Tipo","Antipasto");
            a3.put("Immagine","standard:insalata_di_polpo_e_arancia");
            a3.put("Attributi","Fresca___");
            a3.put("Ingredienti","Polpo lessato:100g___"+
                    "Pomodoro:60g___"+
                    "Finocchio:30g___"+
                    "Spicchi di arancia:2___"+
                    "Succo di arancia:Un cucchiaio___"+
                    "Olio extravergine di oliva:2 cucchiaini___"+
                    "Basilico:Qb___"+
                    "Pepe bianco:Qb___"+
                    "Sake:Qb___");
            a3.put("Difficolta","Media");
            a3.put("Descrizione","Per preparare l’insalata di polpo e arancia tagliate il polpo a tocchetti eliminando o meno, a vostro gusto, le ventose, " +
                    "ma ottenendo sempre il peso di g 100. Tagliate il pomodoro e il finocchio a fettine sottili. " +
                    "Pelate a vivo gli spicchi d’arancia e tagliateli a pezzetti. Disponete sul piatto le fettine di pomodoro, quelle di finocchio e i pezzetti di polpo e di arancia." +
                    " Emulsionate l’olio con il succo dell’arancia, il sale e una macinata di pepe. Condite l’insalata con l’emulsione, poi completate con foglioline di basilico.");
            ric.put(a3);

            p1.put("Nome","Gnocchi alla sorrentina");
            p1.put("Tipo","Primo");
            p1.put("Immagine","standard:gnocchi_alla_sorrentina");
            p1.put("Attributi","Gustosa___"+
                    "Saporita___");
            p1.put("Ingredienti","Gnocchi:500g___"+
                    "Passata di pomodoro:1l___"+
                    "Mozzarella:350g___"+
                    "Parmigiano:50g___"+
                    "Basilico:Qb___"+
                    "Sale:Qb___"+
                    "Olio extravergine di oliva:Qb___"+
                    "Aglio:1 spicchio___");
            p1.put("Difficolta","Facile");
            p1.put("Descrizione","Preparare il sugo facendo soffriggere uno spicchio d'aglio in un po' d'olio, poi aggiungete la passata di pomodoro, il basilico e il sale." +
                    "Far cuocere gli gnocchi in una pentola con abbondante acqua bollente salata." +
                    "Scolateli con un mestolo forato non appena gli gnocchi iniziano a venire a galla." +
                    "Mettete gli gnocchi in padella con il sugo (eliminando lo spicchio d'aglio prima )." +
                    "Aggiungete la mozzarella tagliata a dadini ed il parmigiano" +
                    "Fate saltare gli gnocchi alla sorrentina in padella amalgamando bene il tutto e facendo sciogliere i pezzettini di mozzarella." +
                    "Gurnite il piatto di gnocchi alla sorrentina con una fogliolina di basilico e portate in tavola.");
            ric.put(p1);

            p2.put("Nome","Mezze penne con salsa di zucchine e frutta secca");
            p2.put("Tipo","Primo");
            p2.put("Immagine","standard:mezze_penne_con_salsa_di_zucchine_e_frutta_secca");
            p2.put("Attributi","Semplice___"+
                    "Veloce___"+
                    "Vegetariana");
            p2.put("Ingredienti","Mezze penne:320g___"+
                    "Pomodorini:4___"+
                    "Prosciutto cotto a dadini:80g___"+
                    "Olio extravergine di oliva:5 cucchiai___"+
                    "Zucchine:250g___"+
                    "Granella di mandorle:20g___"+
                    "Prezzemolo tritato:1 cucchiaino___"+
                    "Sale:Qb___"+
                    "Pepe:Qb___"+
                    "Fiori di zucca:6___"+
                    "Pinoli:20g___"+
                    "Pecorino:20g___");
            p2.put("Difficolta","Facile");
            p2.put("Descrizione","Riducete le zucchine a dadini piuttosto piccoli e saltatele in padella a fuoco medio con un cucchiaio di olio e poco sale." +
                    "Unite anche i fiori di zucca tritati e lasciate cuocere per circa 10 minuti. Tostate in padella la granella di mandorle e i pinoli. " +
                    "Riunite nel vaso del frullatore la frutta secca, le zucchine, il prezzemolo, il formaggio e iniziate a frullare, unendo a filo l’olio rimasto, fino ad ottenere una salsa cremosa. " +
                    "Regolate di sapore con sale e pepe. Condite la pasta cotta al dente con la salsa, e mantecatela su fuoco dolce per un minuto. " +
                    "Unite il prosciutto a dadini e i pomodorini a pezzetti, mescolate e servite.");
            ric.put(p2);

            p3.put("Nome","Risotto castelmagno e nocciole");
            p3.put("Tipo","Primo");
            p3.put("Immagine","standard:risotto_castelmagno_e_nocciole");
            p3.put("Attributi","Saporita___"+
                    "Vegetariana");
            p3.put("Ingredienti","Nocciole:100g___"+
                    "Burro:50g___"+
                    "Cipolla:Una___"+
                    "Riso:400g___"+
                    "Formaggio:80g___"+
                    "Brodo vegetale:Qb___"+
                    "Rosmarino:Un rametto___"+
                    "Sale:Qb___"+
                    "Pepe:Qb___");
            p3.put("Difficolta","Difficile");
            p3.put("Descrizione","Tostare le nocciole in padella, facendole cuocere a fuoco abbastanza alto finché non sono leggermente dorate. " +
                    "Toglierle dal fuoco e tritarle nel mixer grossolanamente. Mettere 30 gr di burro nel tegame dove si cucinerà il risotto e rosolarvi la cipolla tagliata sottile, " +
                    "unire il riso e far tostare. Incominciare a bagnare con il brodo caldo. Più o meno a metà cottura, " +
                    "aggiungere le nocciole e metà del castelmagno grattugiato, una macinata di pepe e il rosmarino o la maggiorana. " +
                    "Continuare la cottura bagnando con il brodo fino a che il riso non sarà praticamente pronto. " +
                    "A quel punto, spegnere il fuoco e concludere mantecando con il resto del formaggio e il burro rimanente. " +
                    "Servire con scaglie di castelmagno ed eventualmente qualche nocciola per guarnire.");
            ric.put(p3);

            s1.put("Nome","Cotechino in crosta");
            s1.put("Tipo","Secondo");
            s1.put("Immagine","standard:cotechino_in_crosta");
            s1.put("Attributi","Saporita___"+
                    "Gustosa");
            s1.put("Ingredienti","Cotechino:800g___"+
                    "Foglie di bietola:10___"+
                    "Farina 0:250g___"+
                    "Lievito di birra:Mezza bustina___"+
                    "Uova:1___"+
                    "Zuccher:Qb___"+
                    "Sale:Qb___");
            s1.put("Difficolta","Difficile");
            s1.put("Descrizione","Forare il cotechino in più punti con un grosso ago e avvolgerlo in un foglio di carta forno. " +
                    "Legare l’involucro con lo spago alle due estremità e fare tre legature nella lunghezza, quindi metterli in una casseruola ovale e, " +
                    "appena inizia l’ebollizione, regolare la fiamma al minimo. Farlo cuocere per circa 2 ore a fuoco debole quindi scolarlo e lasciarlo " +
                    "raffreddare avvolto nella carta (anche per un giorno). Setacciare la farina sulla spianatoia e miscelarla con il lievito e mezzo cucchiaino di zucchero. " +
                    "Fare la fontana e aggiungere l’uovo sbattuto, l’olio e 100 ml di acqua appena tiepida. Impastare energicamente per una decina di minuti quindi raccogliere " +
                    "l’impasto a palla e metterlo in una ciotola infarinata dopo avervi fatto due incisioni a croce. Coprire con un panno umido" +
                    " e lasciarlo lievitare in luogo tiepido per un paio di ore. Lavare le foglie di bietola e scottarle per un minuto in acqua in ebollizione. " +
                    "Scolarle, passarle in acqua fredda e allargarle in un panno. Spellare il cotechino e rivestirlo con le foglie di bietola. " +
                    "Rovesciare la pasta sulla spianatoia e, senza più lavorarla, stenderla con il mattarello formando un rettangolo di circa mezzo cm di spessore. " +
                    "Sistemare il cotechino al centro del rettangolo e ritagliare l’eccedenza di pasta tenendo conto di poter avvolgere il cotechino." +
                    " Pennellare con acqua fredda i bordi del rettangolo e avvolgere intorno al cotechino sigillando bene. Trasferire il rotolo in una placca " +
                    "rivestita di carta forno e pennellarlo con il tuorlo diluito con un goccio di acqua. Mettere la placca nel forno a 200° e far cuocere " +
                    "per circa 30’fino a che la pasta sarà ben dorata. A cottura ultimata, trasferire il cotechino su un tagliere. Tagliare a fette spesse.");
            ric.put(s1);

            s2.put("Nome","Baccalà con le fave");
            s2.put("Tipo","Secondo");
            s2.put("Immagine","standard:baccala_con_le_fave");
            s2.put("Attributi","Semplice___"+
                    "Fresca");
            s2.put("Ingredienti","Baccalà:700g___"+
                    "Olio extravergine di oliva:Qb___"+
                    "Cipolla rossa:Una___"+
                    "Pinoli:Qb___"+
                    "Olive taggiasche:Qb___"+
                    "Vino bianco:Qb___"+
                    "Acqua:Qb___"+
                    "Fave:200g___");
            s2.put("Difficolta","Facile");
            s2.put("Descrizione","Tagliare a pezzi il baccalà e rosolarlo in padella con un po’ di olio e toglierlo dalla padella. " +
                    "Tritare la cipolla e rosolarla nella padella del pesce. Unire i pinoli e le olive. " +
                    "Dopo un paio di minuti aggiungere il pesce, sfumare con il vino, aggiungere un goccio di acqua e le fave sgusciate. " +
                    "Cuocere altri 2 o 3 minuri.");
            ric.put(s2);


            s3.put("Nome","Involtini di bresaola");
            s3.put("Tipo","Secondo");
            s3.put("Immagine","standard:involtini_di_bresaola");
            s3.put("Attributi","Saporita___"+
                    "Gustosa___"+
                    "Golosa");
            s3.put("Ingredienti","Olivo extravergine di oliva:Qb___"+
                    "Aglio:1 spicchio___"+
                    "Funghi champignon:200g___"+
                    "Straccchino:200g___"+
                    "Olio tartufato:Qb___"+
                    "Sale:Qb___"+
                    "Bresaola:100g___"+
                    "Prezzemolo:Qb___");
            s3.put("Difficolta","Media");
            s3.put("Descrizione","Affettare gli champignon e rosolarli in padella con olio, aglio, sale e prezzemolo: " +
                    "non devono cuocere molto, ma restare abbastanza sodi e croccanti. " +
                    "Aggiungere al formaggio cremoso l’olio tartufato(un paio di gocce sono sufficienti) e mescolare. " +
                    "Farcire le fettine di bresaola con una cucchiaiata di formaggio al tartufo, richiuderle a involtino" +
                    " senza bisogno di uno stecchino e scottarle sulla griglia della bistecchiera o su una padella antiaderente per pochissimi minuti. " +
                    "Servire gli involtini ben caldi sul letto di funghi.");
            ric.put(s3);

            c1.put("Nome","Insalata Chelsea");
            c1.put("Tipo","Contorno");
            c1.put("Immagine","standard:insalata_chelsea");
            c1.put("Attributi","Sfiziosa___"+
                    "Particolare___");
            c1.put("Ingredienti","Fagioli lessi:Qb___"+
                    "Olive taggiasche:Qb___"+
                    "Tonno:400g___"+
                    "Pomodorini secchi:Qb___"+
                    "Farro lesso:Qb___"+
                    "Olivo extravergine di oliva:Qb___"+
                    "Sale:Qb___"+
                    "Basilico:Qb___");
            c1.put("Difficolta","Facile");
            c1.put("Descrizione","Condire il farro con un po’ di olio. Ripassare i fagiolini in padella, aggiungere i pomodori secchi, le olive e salare. " +
                    "Unire tutto al farro, rosolare un minuto il tonno tagliato a dadini, salarlo e aggiungerlo al farro. Unire i pomodorini e il basilico.");
            ric.put(c1);

            c2.put("Nome","Insalata russa speciale");
            c2.put("Tipo","Contorno");
            c2.put("Immagine","standard:insalata_russa_speciale");
            c2.put("Attributi","Particolare___"+
                    "Golosa");
            c2.put("Ingredienti","Acqua:Qb___"+
                    "Aceto di vino bianco:Un bicchiere___"+
                    "Patate:Qb___"+
                    "Carote:Qb___"+
                    "Giardiniera:Qb___"+
                    "Carciofini:Qb___"+
                    "Funghi:Qb___"+
                    "Cetriolini:Qb___"+
                    "Tonno:150g___"+
                    "Maionese:Qb___");
            c2.put("Difficolta","Facile");
            c2.put("Descrizione","Bollire le carote e le patate in acqua e aceto. Una volta pronte, tagliare a pezzetti piccoli, insieme al resto degli ingredienti, tranne il tonno che va frullato. " +
                    "Amalgamare il tutto con la maionese.");
            ric.put(c2);

            c3.put("Nome","Fiori di zucca alla bottarga");
            c3.put("Tipo","Contorno");
            c3.put("Immagine","standard:fiori_di_zucca_alla_bottarga");
            c3.put("Attributi","Particolare___"+
                    "Sfiziosa___"+
                    "Vegetariana");
            c3.put("Ingredienti","Bottarga:70g___"+
                    "Fiori di zucca:20___"+
                    "Ricotta:300g___"+
                    "Farina 0:120g___"+
                    "Uovo:Uno___"+
                    "Acqua ghiacciata:250ml___"+
                    "Maggiorana:Qb___"+
                    "Olio di semi di arachide:Qb___"+
                    "Sale:Qb___"+
                    "Pepe:Qb___");
            c3.put("Difficolta","Difficile");
            c3.put("Descrizione","Privare i fiori di zucca di una parte del gambo e del pistillo. " +
                    "In una ciotola mescolare bene la ricotta, 30 g di bottarga grattugiata, qualche fogliolina di maggiorana, sale e pepe. " +
                    "Aprire delicatamente i fiori di zucca e riempirli con un cucchiaino di crema di ricotta, quindi richiuderli bene avvicinando i petali tra loro. " +
                    "Sbattere il tuorlo con 2,5 dl di acqua ghiacciata, unire il sale e la farina sempre mescolando. " +
                    "(non usare il frullatore e non lavorare troppo la pastella che, come caratteristica, avrà piccoli grumi di farina) " +
                    "Immergere i fiori di zucca nella pastella e friggerli in abbondante olio ben caldo. " +
                    "Scolarli su della carta da cucina e servirli accompagnati con il resto della bottarga tagliata a fettine.");
            ric.put(c3);

            main.put("big", ric);

       }
       catch (Exception e ){
            Log.d(TAG,"ERRORE NELLA CREAZIONE DEL JSON STANDARD");
           Log.e(TAG,"exception",e);
       }

   }

}
