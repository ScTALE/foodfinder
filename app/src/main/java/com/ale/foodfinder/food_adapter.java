package com.ale.foodfinder;

import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ale on 12/06/16.
 */
public class food_adapter extends RecyclerView.Adapter<food_adapter.foodViewHolder> {
    private static final String TAG="food_adapter";
    public Context context;
    ArrayList<Food_Card> food_cards=new ArrayList<Food_Card>();
    public food_adapter(Context context, ArrayList<Food_Card> food_cards) {
        this.context=context;
        this.food_cards=food_cards;
    }
    @Override
    public foodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_layout,parent,false);

        foodViewHolder fvh=new foodViewHolder(view);
        return fvh;
    }

    @Override
    public void onBindViewHolder(foodViewHolder holder, int position) {
        final Food_Card food=food_cards.get(position);
        String imgsrc=food.getImageid();
        if(imgsrc.contains("standard")) {
            String[] tmp=imgsrc.split(":");
            imgsrc=tmp[1];
            int drawableID = context.getResources().getIdentifier(imgsrc,
                    "drawable",
                    context.getPackageName());
            //holder.food_img.setImageResource(drawableID);
            Picasso.with(context).load(drawableID).error(R.drawable.icona_standard).fit().centerInside().into(holder.food_img);
        }
        else {
            Bitmap bitmap=StringToBitMap(imgsrc);
            holder.food_img.setImageBitmap(bitmap);
        }

        holder.food_name.setText(food.getName());
        holder.food_type.setText(food.getType());
        holder.food_difficolta.setText(food.getDifficolta());
        //cosa accade al click della card
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"Hai cliccato una card");
                mostra_dettagli(food);

            }
        });
    }

    @Override
    public int getItemCount() {
        return food_cards.size();
    }

    public static class foodViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        public ImageView food_img;
        public TextView  food_name,food_type,food_difficolta;
        public foodViewHolder(View view) {
            super(view);
            food_img=(ImageView) view.findViewById(R.id.food_image);
            food_name=(TextView) view.findViewById(R.id.food_name);
            food_type=(TextView) view.findViewById(R.id.food_type);
            food_difficolta=(TextView) view.findViewById(R.id.food_difficolta);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
    public void mostra_dettagli(Food_Card food) {
        DBhelper database = ((MainActivity)context).getDatabase();
        String nome=food.getName();
        String tipo=food.getType();
        String difficolta=food.getDifficolta();
        String attributi="";
        String ingredienti="";
        String descrizione="";
        String immagine="";
        ArrayList<String> info=new ArrayList<String>();
        Cursor cursor=database.ottieni_tutto();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            //Questi controlli si basano sul fatto che non esiste una ricetta con nome tipo e difficoltà uguali
            if(cursor.getString(cursor.getColumnIndex(DBhelper.COL_NOME)).equals(nome)) {
                if(cursor.getString(cursor.getColumnIndex(DBhelper.COL_TIPO)).equals(tipo)) {
                    if(cursor.getString(cursor.getColumnIndex(DBhelper.COL_DIFFICOLTA)).equals(difficolta)) {
                        attributi=cursor.getString(cursor.getColumnIndex(DBhelper.COL_ATTRIBUTI));
                        ingredienti=cursor.getString(cursor.getColumnIndex(DBhelper.COL_INGREDIENTI));
                        descrizione=cursor.getString(cursor.getColumnIndex(DBhelper.COL_DESCRIZIONE));
                        immagine=cursor.getString(cursor.getColumnIndex(DBhelper.COL_IMMAGINE));
                    }
                }
            }

            cursor.moveToNext();
        }
        cursor.close();

        info.add(nome);
        info.add(tipo);
        info.add(difficolta);
        info.add(attributi);
        info.add(ingredienti);
        info.add(descrizione);
        info.add(immagine);

        //Fragment
        android.app.FragmentManager manager = ((MainActivity) context).getFragmentManager();
        FragmentTransaction FT = manager.beginTransaction();
        Frag_Dettagli FL = new Frag_Dettagli();


        //Bundle
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("key",info);


        FL.setArguments(bundle);

        FT.replace(R.id.posto_per_fragment,FL,"Frag_Dettagli");
        FT.addToBackStack("Frag_Dettagli");
        FT.commit();
        manager.executePendingTransactions();

        //Aggiorno menù
        ((MainActivity)context).invalidateOptionsMenu();
    }
    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
}
