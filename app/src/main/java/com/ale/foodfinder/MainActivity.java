package com.ale.foodfinder;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.renderscript.RSIllegalArgumentException;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ViewUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public DBhelper database;
    public CallbackManager callbackManager;
    public ShareDialog shareDialog;
    public ShareButton shareButton;
    //public ArrayList<Ricetta> lista_ricette=new ArrayList<Ricetta>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareButton=new ShareButton(this);
        //optional
        //shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() { });

        //key_hash();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //Gestione Facebook login
        ArrayList<String> permessi=new ArrayList<String>();
        permessi.add("user_friends");
        permessi.add("public_profile");
        permessi.add("email");
        //permessi.add("publish_actions");
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(permessi);

        get_login_details(loginButton);
        /*shareButton=(ShareButton) findViewById(R.id.share_button);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharePhotoContent content=genera_share_photo_content();
                ShareApi.share(content,null);//ShareApi is important
            }
        });*/
        //sharePhotoContent=genera_share_photo_content();
        //shareButton.setShareContent(sharePhotoContent);

        //Posizionamento Frag_Principale
        android.app.FragmentManager FM = getFragmentManager();
        FragmentTransaction FT = FM.beginTransaction();
        Frag_Principale FP = new Frag_Principale();
        FT.add(R.id.posto_per_fragment, FP,"Frag_Principale");
        FT.commit();
        //FM.executePendingTransactions();


        //Aggiorno pulsante menù
        invalidateOptionsMenu();

        //Creazione database
        database = new DBhelper(getApplicationContext());

        //Controllo se il database è vuoto
        Cursor controllo = database.ottieni_tutto();
        if (controllo != null) {
            if (controllo.moveToFirst()) {
                //Database pieno
                Log.d(TAG, "Cursor pieno");
                /*Cursor cursor = database.ottieni_tutto();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Log.d(TAG, cursor.getString(cursor.getColumnIndex(DBhelper.COL_NOME)));
                    cursor.moveToNext();
                }
                cursor.close();*/
            } else {
                //Database vuoto
                Log.d(TAG, "Cursor vuoto");
                Json_Standard js = new Json_Standard(getApplicationContext());
                try {
                    //stampo il json delle ricette
                    //Log.d(TAG, js.main.toString(4));
                    int i;
                    JSONArray jArray = js.main.getJSONArray("big");
                    Log.d(TAG, "Ci sono memorizzate " + jArray.length()+ " ricette");
                    //Aggiungo ogni ricetta dentro al database-
                    for (i = 0; i < jArray.length(); i++) {
                        JSONObject ciclo = jArray.getJSONObject(i);
                        String nome = ciclo.getString("Nome");
                        String tipo = ciclo.getString("Tipo");
                        String immagine = ciclo.getString("Immagine");
                        String attributi = ciclo.getString("Attributi");
                        String ingredienti = ciclo.getString("Ingredienti");
                        String difficolta = ciclo.getString("Difficolta");
                        String descrizione = ciclo.getString("Descrizione");
                        Ricetta ric = new Ricetta(nome, tipo, immagine, attributi, ingredienti, difficolta, descrizione);
                        database.inserisci_ricetta(ric);
                        //lista_ricette.add(ric);

                    }
                    //Log.d(TAG," "+js.main.length()+" "+jArray.length());


                } catch (Exception e) {
                    Log.d(TAG, "ERRORE IN INSERIMENTO JSON STANDARD");
                }
            }
            controllo.close();
        } else {
            //La query non ritorna niente
            Log.d(TAG, "Cursor nullo");
        }

        //Funzioni necessarie per android 6+
        controllo_permessi("ReadExt");
        controllo_permessi("WriteExt");
        controllo_permessi("Camera");
        controllo_permessi("Flash");
        controllo_permessi("Internet");


        /*Intent intent=new Intent(MainActivity.this, SharingActivity.class);
        startActivity(intent);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        //controllo in che fragment sono e rendo disponibili determinate funzioni nel menù
        String nome_current_fragment=ottieni_nome_fragment_corrente();
        if(!(nome_current_fragment.matches("Frag_Dettagli"))) {
            menu.findItem(R.id.condividi).setVisible(false);
            menu.findItem(R.id.genera_pdf).setVisible(false);
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.condividi) {
            try {
                if (ShareDialog.canShow(ShareLinkContent.class)) {

                    Frag_Dettagli current=(Frag_Dettagli)ottieni_fragment_corrente();
                    String[] elementi_da_condividere=current.ottieni_per_condividere();

                    food_adapter vuoto=new food_adapter(getApplicationContext(),null);
                    Bitmap image = vuoto.StringToBitMap(elementi_da_condividere[6]);
                    String url_raspb=elementi_da_condividere[6];
                    if(elementi_da_condividere[6].contains("standard")) {
                        url_raspb=elementi_da_condividere[6].split(":")[1];
                    }
                    url_raspb="http://raspb-ale.noip.me/link_usb/drawable/"+url_raspb+".jpg";
                    //Log.d(TAG,""+url_raspb);
                    /*SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(image)
                            .setCaption("KIK")
                            .build();
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();
                    */
                    ShareLinkContent linkContent;
                    if(elementi_da_condividere[6].contains("standard")) {
                        linkContent = new ShareLinkContent.Builder()
                                .setContentTitle(elementi_da_condividere[0])
                                .setImageUrl(Uri.parse(url_raspb))
                                .setContentDescription(
                                        "Ricetta " + elementi_da_condividere[2])
                                .setContentUrl(Uri.parse(url_raspb))
                                .build();
                    }
                    else {
                        Ricetta v=new Ricetta("","","","","","","");
                        String[] ritorno2=v.ottieni_attributi(elementi_da_condividere[3]);
                        String attr_set="";
                        for(int i=0;i<ritorno2.length;i++) {
                            attr_set=attr_set+ritorno2[i]+" ";
                        }
                        linkContent = new ShareLinkContent.Builder()
                                .setContentTitle(elementi_da_condividere[0])
                                .setContentDescription(
                                        "Ricetta " + elementi_da_condividere[2]+
                                                "\nQuesta ricetta è "+ attr_set)
                                .setContentUrl(Uri.parse("http://raspb-ale.noip.me"))
                                .build();
                    }
                    shareDialog.show(linkContent);  // Show facebook ShareDialog
                    //ShareApi.share(content,null);
                    //Toast.makeText(getApplicationContext(),"Codivisione avvenuta",Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Errore durante la condivisione", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        else {
            if(id==R.id.genera_pdf) {
                try {
                    Frag_Dettagli current=(Frag_Dettagli)ottieni_fragment_corrente();
                    String[] elementi_da_condividere=current.ottieni_per_condividere();
                    creaPDF(elementi_da_condividere);
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (DocumentException d){
                    d.printStackTrace();
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            Log.d(TAG, "Back premuto, count=0");
            super.onBackPressed();
        } else {
            Log.d(TAG, "Back premuto, pop");
            getFragmentManager().popBackStack();

            //Aggiorno pulsante menù
            invalidateOptionsMenu();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
        Log.e(TAG,data.toString());

    }

    public DBhelper getDatabase() {
        return database;
    }

    //public ArrayList<Ricetta> getListaRicette() { return lista_ricette;}

    protected void get_login_details(LoginButton loginButton) {
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                //Toast.makeText(getApplicationContext(),"SI",Toast.LENGTH_LONG).show();
                Log.d(TAG,"Login avvenuto con successo");
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(getApplicationContext(),"CANCEL",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getApplicationContext(),"NO",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void controllo_permessi(String permesso) {
        if(permesso.matches("ReadExt")) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 25);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                //nothing
            }
        }
        else {
            if (permesso.matches("WriteExt")) {
                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Show an expanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 30);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    //nothing
                }
            }
            else {
                if (permesso.matches("Camera")) {
                    // Here, thisActivity is the current activity
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.CAMERA)) {

                            // Show an expanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                        } else {

                            // No explanation needed, we can request the permission.

                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 35);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    } else {
                        //nothing
                    }
                }
                else if (permesso.matches("Flash")) {
                    // Here, thisActivity is the current activity
                    /*if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.FLASHLIGHT) != PackageManager.PERMISSION_GRANTED) {

                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.FLASHLIGHT)) {

                            // Show an expanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                        } else {

                            // No explanation needed, we can request the permission.

                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.FLASHLIGHT}, 40);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    } else {
                        //nothing
                    }*/
                }
                else {
                    if(permesso.matches("Internet")) {
                        // Here, thisActivity is the current activity
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.INTERNET)) {

                                // Show an expanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.INTERNET}, 45);

                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        } else {
                            //nothing
                        }
                    }
                }
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[],int[] grantResults) {
        //Toast.makeText(MainActivity.this, "ciao", Toast.LENGTH_LONG).show();
        switch (requestCode) {
            case 25: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 30: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 35: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 40: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case 45: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    public String ottieni_nome_fragment_corrente() {
        String ritorno="FRAGMENT_NON_RICONOSCIUTO";

        android.app.Fragment currentFragment = getFragmentManager().findFragmentById(R.id.posto_per_fragment);
        if (currentFragment instanceof Frag_Principale) {
            ritorno="Frag_Principale";
        }
        else {
            if(currentFragment instanceof Frag_Lista) {
                ritorno="Frag_Lista";
            }
            else {
                if(currentFragment instanceof  Frag_Nuova_Ricetta) {
                    ritorno="Nuova_Ricetta";
                }
                else if(currentFragment instanceof Frag_Dettagli) {
                    ritorno="Frag_Dettagli";
                }
            }
        }

        return ritorno;
    }
    public android.app.Fragment ottieni_fragment_corrente() {
        android.app.Fragment currentFragment = getFragmentManager().findFragmentById(R.id.posto_per_fragment);
        return  currentFragment;
    }
    public SharePhotoContent genera_share_photo_content() {
        Frag_Dettagli current=(Frag_Dettagli)ottieni_fragment_corrente();
        String[] elementi_da_condividere=current.ottieni_per_condividere();

        food_adapter vuoto=new food_adapter(getApplicationContext(),null);
        Bitmap image = vuoto.StringToBitMap(elementi_da_condividere[6]);

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("LOL")
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .setContentUrl(Uri.parse("LOL"))
                .build();
        return content;
    }
    public void creaPDF(String[] elementi) throws FileNotFoundException, DocumentException {
        Log.d(TAG, "sono qui");

        String pathname = Environment.getExternalStorageDirectory().getAbsolutePath()
                + File.separator + "FoodFinder_app_pdf";

        File pdfFolder = new File(pathname);
        if(!pdfFolder.exists()) {
            Log.d(TAG, "la cartella non esisteva");
            if (pdfFolder.mkdir()) {
                Log.d(TAG,"ho creato la cartella nella maniera corretta");
            }
            else
                Log.d(TAG, "errore di qualche tipo nella creazione della cartella");
        }
        else {
            Log.d(TAG, "la cartella esisteva gia");
        }

        if (!pdfFolder.exists()) {
            pdfFolder.mkdir();
            Log.i(TAG, "Pdf Directory created");
        }

        //Create time stamp
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);

        File myFile = new File(pdfFolder + timeStamp + ".pdf");

        OutputStream output = new FileOutputStream(myFile);

        Document document = new Document();

        PdfWriter.getInstance(document, output);

        //Step 3
        document.open();

        //Step 4 Add content
        //document.add(new Paragraph(mSubjectEditText.getText().toString()));
        //document.add(new Paragraph(mBodyEditText.getText().toString()));



        String nome=elementi[0];
        String tipo=elementi[1];
        String difficolta=elementi[2];
        String attributi=elementi[3];
        String ingredienti=elementi[4];
        String descrizione=elementi[5];
        String immagine=elementi[6];

        Ricetta vuota=new Ricetta("","","","","","","");
        String ing_set="";
        String[][] ritorno=vuota.ottieni_ingredienti_e_quantita(ingredienti);
        int i,j;
        for(i=0;i<ritorno.length;i++) {
            ing_set=ing_set+ritorno[i][0]+
                    ":"+
                    ritorno[i][1]+
                    "\n";
        }
        String[] ritorno2=vuota.ottieni_attributi(attributi);
        String attr_set="";
        for(i=0;i<ritorno2.length;i++) {
            attr_set=attr_set+ritorno2[i]+" ";
        }
        try {
            if (immagine.contains("standard")) {
                String tmp[] = immagine.split(":");
                String srcImmagine = tmp[1];
                int drawableID = getResources().getIdentifier(srcImmagine,
                        "drawable",
                        getPackageName());
                Drawable drawable = getResources().getDrawable(drawableID, null);
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                //byte[] bitmapData = stream.toByteArray();
                Image image = Image.getInstance(stream.toByteArray());
                image.setAlignment(Image.MIDDLE);
                image.scaleToFit(250f,250f);
                document.add(image);
            } else {
                food_adapter f_vuoto = new food_adapter(getApplicationContext(), new ArrayList<Food_Card>());
                Bitmap bitmap = f_vuoto.StringToBitMap(immagine);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Image myimage = Image.getInstance(stream.toByteArray());
                myimage.setAlignment(Image.MIDDLE);
                myimage.scaleToFit(250f,250f);
                document.add(myimage);

            }
        }
        catch (Exception e) {
            Log.e(TAG,"Errore pdf immagine");
        }



        // Left
        Paragraph paragraph = new Paragraph("Nome ricetta: "+nome);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);
        // Centered
        paragraph = new Paragraph("Tipo: "+tipo);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);
        // Left
        paragraph = new Paragraph("Difficoltà: "+difficolta);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);
        // Left with indentation
        paragraph = new Paragraph("Attributi: "+attr_set);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);

        paragraph = new Paragraph("Ingredienti:\n"+ing_set);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);

        paragraph = new Paragraph("Descrizione:\n"+descrizione);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);



        /*paragraph = new Paragraph(timeStamp);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setIndentationLeft(50);
        document.add(paragraph);*/


        //Step 5: Close the document
        document.close();
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
            startActivity(intent);
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(),"Non è stata trovata alcuna applicazione in grado di aprire il pdf.\nE' stato comunque salvato.",Toast.LENGTH_LONG).show();
        }


    }
    public void key_hash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.ale.foodfinder",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}

