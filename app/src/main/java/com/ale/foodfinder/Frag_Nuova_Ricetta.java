package com.ale.foodfinder;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.share.widget.ShareButton;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by ale on 10/06/16.
 */
public class Frag_Nuova_Ricetta  extends Fragment{
    public static final int CAMERA_REQUEST=0;
    private static final String TAG = "Frag_Nuova_Ricetta";
    public int numero_ingredienti=0;
    public int numero_attributi=0;
    //Array per gli id delle edittext degli ingredienti, numero massimo di ingredienti 50
    public int EditTextIngredientiIdArray[]=new int[50];
    public int EditTextAttributiIdArray[]=new int[50];
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_nuova_ricetta, container, false);
        setHasOptionsMenu(true);
        return (v);
    }
    @Override
    public void onViewCreated (View view, Bundle savedIstanceState) {

        //Azioni per generare pulsante menù ingredienti
        FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.add_ing);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getActivity();
                LinearLayout linearLayout=(LinearLayout) getView().findViewById(R.id.ing_agg);
                EditText ed=new EditText(context);
                ed.setHint("Ingrediente:\nQuantità");
                LinearLayout.LayoutParams lParamsMW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                int id=getView().generateViewId();
                ed.setId(id);
                ed.setTag(numero_ingredienti);
                EditTextIngredientiIdArray[numero_ingredienti]=id;
                ed.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                ed.setLayoutParams(lParamsMW);
                linearLayout.addView(ed);
                numero_ingredienti++;
            }
        });

        //Azioni per generare pulsante menù attributi
        FloatingActionButton fab2 = (FloatingActionButton) getView().findViewById(R.id.add_attr);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = getActivity();
                LinearLayout linearLayout=(LinearLayout) getView().findViewById(R.id.attr_agg);
                EditText ed=new EditText(context);
                ed.setHint("Attributo \naggiuntivo");
                LinearLayout.LayoutParams lParamsMW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                int id=getView().generateViewId();
                ed.setId(id);
                ed.setTag(numero_attributi);
                EditTextAttributiIdArray[numero_attributi]=id;
                ed.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                ed.setLayoutParams(lParamsMW);
                linearLayout.addView(ed);
                numero_attributi++;
            }
        });

        //Controllo campi e salvataggio
        FloatingActionButton fab_sav = (FloatingActionButton) getView().findViewById(R.id.salva_ricetta);
        fab_sav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    int pronto_per_salvare=0;
                    String nome="",tipo="",difficolta="",ingredienti="",attributi="",descrizione="",immagine="";
                    try {
                        EditText ntemp = (EditText) getView().findViewById(R.id.nuovo_nome);
                        nome = ntemp.getText().toString();
                        if(nome.matches("")) {
                            Log.d(TAG,"Nome vuoto");
                            pronto_per_salvare=1;
                        }
                    }
                    catch (Exception e) {
                        Log.d(TAG,"Errore Nome");
                        pronto_per_salvare=1;
                    }
                    try {
                        Spinner stemp = (Spinner) getView().findViewById(R.id.nuovo_tipo);
                        tipo = stemp.getSelectedItem().toString();
                        if(tipo.matches("")) {
                            Log.d(TAG,"Tipo vuoto");
                            pronto_per_salvare=1;
                        }
                    }
                    catch (Exception e) {
                        Log.d(TAG,"Errore tipo");
                        pronto_per_salvare=1;
                    }
                    try {
                        Spinner dtemp = (Spinner) getView().findViewById(R.id.nuova_difficolta);
                        difficolta = dtemp.getSelectedItem().toString();
                        if(difficolta.matches("")) {
                            Log.d(TAG,"Difficolta vuoto");
                            pronto_per_salvare=1;
                        }
                    }
                    catch (Exception e) {
                        Log.d(TAG,"Errore Difficoltà");
                        pronto_per_salvare=1;
                    }
                    try {
                        int controllo_ing=controllo_ingredienti();
                        if(controllo_ing==0) {
                            Log.d(TAG,"Gli ingredienti van bene");
                            ingredienti=unisci_campi_ingredienti();
                        }
                        else {
                            Log.d(TAG,"Errore controllo_ingredienti");
                            pronto_per_salvare=1;
                        }

                    }
                    catch (Exception e) {
                        Log.d(TAG,"Errore ingredienti");
                        pronto_per_salvare=1;
                    }

                    try {
                        attributi=unisci_campi_attributi();
                        if(attributi.matches("")) {
                            Log.d(TAG,"Attributi vuoto");
                            pronto_per_salvare=1;
                        }

                    }
                    catch (Exception e) {
                        Log.d(TAG,"Errore unisci campi ingredienti");
                        pronto_per_salvare=1;
                    }

                    try {
                        EditText detemp = (EditText) getView().findViewById(R.id.nuova_descrizione);
                        descrizione = detemp.getText().toString();
                        if(descrizione.matches("")) {
                            Log.d(TAG,"Descrizione vuota");
                            pronto_per_salvare=1;
                        }
                    }
                    catch (Exception e) {
                        Log.d(TAG,"Errore in descrizione");
                        pronto_per_salvare=1;
                    }
                    try {
                        ImageView temp=(ImageView) getView().findViewById(R.id.foto);
                        Bitmap bitmap=((BitmapDrawable)temp.getDrawable()).getBitmap();
                        immagine=BitMapToString(bitmap);
                    }
                    catch (Exception e) {
                        Log.d(TAG,"Errore Immagine");
                        //pronto_per_salvare=1;
                    }

                //Salvo e ritorno al fragment lista
                if(pronto_per_salvare==0) {
                    Log.d(TAG,"Tutto ok, salvo");
                    Ricetta ric=new Ricetta(nome,tipo,immagine,attributi,ingredienti,difficolta,descrizione);
                    DBhelper database = ((MainActivity)getActivity()).getDatabase();
                    database.inserisci_ricetta(ric);
                    //ArrayList<Ricetta> list=((MainActivity)getActivity()).getListaRicette();
                    //list.add(ric);

                    android.app.FragmentManager FM=getFragmentManager();
                    FragmentTransaction FT=FM.beginTransaction();
                    Frag_Lista FL=new Frag_Lista();
                    FT.replace(R.id.posto_per_fragment,FL,"Frag_Lista");
                    FT.commit();
                    //FM.executePendingTransactions();

                    //Aggiorno menù
                    getActivity().invalidateOptionsMenu();

                }
                else {
                    Toast.makeText(getActivity(),"Ci sono dei campi con informazioni non ben strutturate",Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Foto
        FloatingActionButton fab_fot = (FloatingActionButton) getView().findViewById(R.id.aggiungi_foto);
        fab_fot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View dialog_scegli=View.inflate(getActivity(),R.layout.finestra_foto_o_galleria,null);
                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                builder.setTitle("Vuoi scattare la foto o sceglierla dalla galleria?");
                builder.setView(dialog_scegli);
                builder.setCancelable(true);
                builder.setPositiveButton("Scatta", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent camera_intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(camera_intent,CAMERA_REQUEST);
                    }
                });
                builder.setNegativeButton("Cancella", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setNeutralButton("Galleria", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent=new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent,555);
                        dialog.cancel();
                    }
                });
                builder.show();
                /*Intent camera_intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(camera_intent,CAMERA_REQUEST);*/

            }
        });
        /*ShareButton shareButton=(ShareButton)getActivity().findViewById(R.id.share_button);
        shareButton.setVisibility(View.GONE);*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"ciao\n"+requestCode+"\n"+resultCode+"\n"+data);
        if(requestCode==CAMERA_REQUEST) {
            if(resultCode== Activity.RESULT_OK) {
                Log.d(TAG,"Foto Scattata");
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ImageView imageView=(ImageView)getView().findViewById(R.id.foto);
                imageView.setImageBitmap(photo);
            }
            else {
                Log.d(TAG,"Niente foto");
            }
        }
        else {
            if(requestCode==555) {
                if(resultCode==Activity.RESULT_OK) {
                    Uri selectedImage=data.getData();
                    String[] filePathColumn= {MediaStore.Images.Media.DATA};
                    Cursor cursor=getActivity().getContentResolver().query(selectedImage,filePathColumn,null,null,null);
                    if(cursor != null) {
                        if (cursor.moveToFirst()) {
                            /*int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String picurePath = cursor.getString(columnIndex);
                            cursor.close();
                            Bitmap photo = BitmapFactory.decodeFile(picurePath);*/
                            ImageView imageView = (ImageView) getView().findViewById(R.id.foto);
                            //imageView.setImageBitmap(photo);
                            Picasso.with(getActivity()).load(selectedImage).fit().centerInside().into(imageView);
                        }
                    }
                }
            }
            else Log.d(TAG,"Request non riconosciuta");
        }
    }

    public int controllo_ingredienti() {
        EditText e_d=(EditText)getView().findViewById(R.id.nuovo_ingrediente);
        LinearLayout l2=(LinearLayout)getView().findViewById(R.id.ing_agg);

        String s_e_d=e_d.getText().toString();
        if (s_e_d.matches("")) {
            return (1);
        }
        int k, count = 0;
        for (k = 0; k < s_e_d.length(); k++) {
            if (s_e_d.charAt(k) == ':') count++;
        }
        if (count != 1) return (1);

        int i;
        for(i=0;i<l2.getChildCount();i++) {

            EditText temp = (EditText) l2.getChildAt(i);
            String s = temp.getText().toString();
            if (s.matches("")) {
                return (1);
            }
            int k2, count2 = 0;
            for (k2 = 0; k2 < s.length(); k2++) {
                if (s.charAt(k2) == ':') count2++;
            }
            if (count2 != 1) return (1);


        }
        return(0);
    }
    public String unisci_campi_ingredienti() {
        String finale,tmp;
        EditText e_s=(EditText)getView().findViewById(R.id.nuovo_ingrediente);
        tmp=e_s.getText().toString();
        finale=tmp+"___";
        LinearLayout l2=(LinearLayout)getView().findViewById(R.id.ing_agg);
        int i;
        for(i=0;i<l2.getChildCount();i++) {
            EditText et=(EditText)l2.getChildAt(i);
            String s =et.getText().toString();
            finale=finale+s+"___";
        }
        return (finale);
    }

    public String unisci_campi_attributi() {
        String finale, tmp;
        EditText e_s=(EditText)getView().findViewById(R.id.nuovo_attributo);
        tmp=e_s.getText().toString();
        finale=tmp+"___";
        LinearLayout l2=(LinearLayout)getView().findViewById(R.id.attr_agg);
        int i;
        for(i=0;i<l2.getChildCount();i++) {
            EditText et=(EditText)l2.getChildAt(i);
            String s =et.getText().toString();
            finale=finale+s+"___";
        }

        return (finale);
    }
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}
