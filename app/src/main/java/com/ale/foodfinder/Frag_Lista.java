package com.ale.foodfinder;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.widget.ShareButton;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by ale on 07/06/16.
 */
public class Frag_Lista extends Fragment {
    private static final String TAG="Frag_Lista";
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Food_Card> list=new ArrayList<Food_Card>();
    String[] name=new String[50];
    String[] tipo=new String[50];
    String[] difficolta=new String[50];
    String[] ingredienti=new String[50];
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_lista, container, false);
        setHasOptionsMenu(true);
        return(v);
    }
    @Override
    public void onViewCreated (View view, Bundle savedIstanceState) {

        //Azioni per generare pulsante menù
        FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                android.app.FragmentManager FM = getFragmentManager();
                FragmentTransaction FT = FM.beginTransaction();
                Frag_Nuova_Ricetta FL = new Frag_Nuova_Ricetta();
                FT.replace(R.id.posto_per_fragment,FL,"Frag_Nuova_Ricetta");
                FT.addToBackStack("Frag_Nuova_Ricetta");
                FT.commit();
                //FM.executePendingTransactions();

                //Aggiorno menù
                getActivity().invalidateOptionsMenu();
            }
        });

        FloatingActionButton fil  = (FloatingActionButton) getView().findViewById(R.id.filtri);
        fil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finestra_dialogo_filtri();
            }
        });

        Context context = getActivity();
        DBhelper database = ((MainActivity)getActivity()).getDatabase();
        Cursor cursor=database.ottieni_tutto();
        genera_lista(cursor);

        /*ShareButton shareButton=(ShareButton)getActivity().findViewById(R.id.share_button);
        shareButton.setVisibility(View.GONE);*/

    }

    public void genera_lista(Cursor cursor) {
        list.clear();
        if(cursor!=null) {
            if(cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    //Log.d(TAG,cursor.getString(cursor.getColumnIndex(DBhelper.COL_NOME)));
                    String nome = cursor.getString(cursor.getColumnIndex(DBhelper.COL_NOME));
                    String tipo = cursor.getString(cursor.getColumnIndex(DBhelper.COL_TIPO));
                    String difficolta = cursor.getString(cursor.getColumnIndex(DBhelper.COL_DIFFICOLTA));
                    String immagine = cursor.getString(cursor.getColumnIndex(DBhelper.COL_IMMAGINE));
                    Food_Card food_card = new Food_Card(immagine, nome, tipo, difficolta);
                    list.add(food_card);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            else {
                Log.d(TAG,"Cursor vuoto");
                Toast.makeText(getActivity(),"Non è stata trovata alcuna ricetta con gli elementi da te richiesti",Toast.LENGTH_LONG).show();
            }
        }
        else {
            Log.d(TAG,"Cursor nullo");
            Toast.makeText(getActivity(),"Non è stata trovata alcuna ricetta con i parametri da te inseriti",Toast.LENGTH_LONG).show();
        }



        recyclerView = (RecyclerView)getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        adapter=new food_adapter(getActivity(),list);
        recyclerView.setAdapter(adapter);
    }


    public void finestra_dialogo_filtri() {
        final View dialog_filtri=View.inflate(getActivity(),R.layout.finestra_filtri,null);
        final LinearLayout l= (LinearLayout) dialog_filtri.findViewById(R.id.linear_con_filtri);
        final LinearLayout l2=(LinearLayout) dialog_filtri.findViewById(R.id.linear_text_ingredienti);
        final LinearLayout l3=(LinearLayout) dialog_filtri.findViewById(R.id.linear_text_attributi);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Filtra per");
        builder.setView(dialog_filtri);
        builder.setCancelable(true);
        //Azione filtra
        builder.setPositiveButton("Filtra",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                String[] parametri=new String[l.getChildCount()];
                String[] parametri_ingredienti=new String[l2.getChildCount()];
                String[] parametri_attributi=new String[l3.getChildCount()];
                //Spinner Tipo e difficoltà
                int i;
                for(i=0;i<l.getChildCount();i++) {

                    if(l.getChildAt(i) instanceof Spinner) {
                        Spinner temp=(Spinner)l.getChildAt(i);
                        if(temp.getVisibility()==View.VISIBLE) {
                            parametri[i] = temp.getSelectedItem().toString();
                        }
                    }
                }
                //TextView ingredienti
                int j;
                for(j=0;j<l2.getChildCount();j++) {
                    if(l2.getChildAt(j) instanceof TextView) {
                        TextView temp=(TextView) l2.getChildAt(j);
                        if(temp.getVisibility()==View.VISIBLE) {
                            if(!(temp.getText().toString().equals("Tocca per aggiungere un ingrediente"))){
                                parametri_ingredienti[j]=temp.getText().toString();
                            }
                        }
                    }
                }
                //TextView attributi
                int k;
                for(k=0;k<l3.getChildCount();k++) {
                    if(l3.getChildAt(k) instanceof TextView) {
                        TextView temp=(TextView) l3.getChildAt(k);
                        if(temp.getVisibility()==View.VISIBLE) {
                            if(!(temp.getText().toString().equals("Tocca per aggiungere un attributo"))) {
                                parametri_attributi[k]=temp.getText().toString();
                            }
                        }
                    }
                }

                dialog.cancel();
                filtra_lista(parametri,parametri_ingredienti,parametri_attributi);
            }
        });
        //Azione cancella
        builder.setNegativeButton("Chiudi",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });
        //Azione azzera
        builder.setNeutralButton("Azzera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                String[] tmp2=new String[100];
                String[] tmp3=new String[100];
                filtra_lista(new String[1],tmp2,tmp3);
            }
        });

        builder.show();

        gestisci_checkbox(dialog_filtri);

    }
    public void gestisci_checkbox(final View v) {
        //b è il "backup" della view
        final View b=v;
        CheckBox checkBoxTipo=(CheckBox) v.findViewById(R.id.check_tipo);
        checkBoxTipo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner s=(Spinner) v.findViewById(R.id.filtro_tipo);
                if(isChecked) {
                    s.setVisibility(View.VISIBLE);
                }
                else {
                    s.setVisibility(View.GONE);
                }
            }
        });

        CheckBox checkBoxDifficolta=(CheckBox) v.findViewById(R.id.check_difficolta);
        checkBoxDifficolta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Spinner s=(Spinner) v.findViewById(R.id.filtro_difficolta);
                if(isChecked) {
                    s.setVisibility(View.VISIBLE);
                }
                else {
                    s.setVisibility(View.GONE);
                }
            }
        });

        CheckBox checkBoxIngredienti=(CheckBox) v.findViewById(R.id.check_ingredienti);
        checkBoxIngredienti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //ListView l=(ListView)v.findViewById(R.id.listview_ingredienti);
                LinearLayout l=(LinearLayout)v.findViewById(R.id.linear_text_ingredienti);
                final TextView t1=(TextView)v.findViewById(R.id.filtro_ing_1);
                final TextView t2=(TextView)v.findViewById(R.id.filtro_ing_2);
                final TextView t3=(TextView)v.findViewById(R.id.filtro_ing_3);
                if(isChecked) {
                    l.setVisibility(View.VISIBLE);


                    t1.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            gestisci_listView(b,t1,0);
                            return false;
                        }
                    });

                    t2.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            gestisci_listView(b,t2,0);
                            return false;
                        }
                    });
                    t3.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            gestisci_listView(b,t3,0);
                            return false;
                        }
                    });

                }
                else {
                    l.setVisibility(View.GONE);
                    t1.setOnTouchListener(null);
                    t2.setOnTouchListener(null);
                    t3.setOnTouchListener(null);
                }
            }
        });

        CheckBox checkBoxAttributi=(CheckBox) v.findViewById(R.id.check_attributi);
        checkBoxAttributi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LinearLayout linearLayout=(LinearLayout) v.findViewById(R.id.linear_text_attributi);
                final TextView t1=(TextView)v.findViewById(R.id.filtro_attr_1);
                final TextView t2=(TextView)v.findViewById(R.id.filtro_attr_2);
                final TextView t3=(TextView)v.findViewById(R.id.filtro_attr_3);
                if(isChecked) {
                    linearLayout.setVisibility(View.VISIBLE);
                    t1.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            gestisci_listView(b,t1,1);
                            return false;
                        }
                    });
                    t2.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            gestisci_listView(b,t2,1);
                            return false;
                        }
                    });
                    t3.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            gestisci_listView(b,t3,1);
                            return false;
                        }
                    });
                }
                else {
                    linearLayout.setVisibility(View.GONE);
                    t1.setOnTouchListener(null);
                    t2.setOnTouchListener(null);
                    t3.setOnTouchListener(null);
                }
            }
        });

    }
    public void filtra_lista(String[] parametri,String[] parametri_ingredienti, String[] parametri_attributi) {
        //Parametri 0 tipo
        //Parametri 1 difficoltà
        Cursor cursor=((MainActivity)getActivity()).database.query_filtri(parametri,parametri_ingredienti,parametri_attributi);

        genera_lista(cursor);
    }
    public ArrayList<String> ottieni_ingredienti_disponibili() {
        ArrayList<String> ritorno=new ArrayList<String>();
        DBhelper dBhelper=new DBhelper(getActivity());
        Cursor cursor=dBhelper.ottieni_tutto();
        if(cursor!=null) {
            if(cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    String ing_lunghi=cursor.getString(cursor.getColumnIndex(DBhelper.COL_INGREDIENTI));
                    Ricetta ricetta=new Ricetta("","","","","","","");
                    String ing_e_quantita[][]=ricetta.ottieni_ingredienti_e_quantita(ing_lunghi);
                    for(int i=0;i<ing_e_quantita.length;i++) {
                        if (!(ritorno.contains(ing_e_quantita[i][0]))) {
                            ritorno.add(ing_e_quantita[i][0]);
                        }
                    }
                    cursor.moveToNext();
                }
            }
        }
        cursor.close();

        return ritorno;
    }
    public ArrayList<String> ottieni_attributi_disponibili() {
        ArrayList<String> ritorno=new ArrayList<String>();
        DBhelper dBhelper=new DBhelper(getActivity());
        Cursor cursor=dBhelper.ottieni_tutto();
        if(cursor!=null) {
            if(cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    String attr_lunghi=cursor.getString(cursor.getColumnIndex(DBhelper.COL_ATTRIBUTI));
                    Ricetta ricetta=new Ricetta("","","","","","","");
                    String[] attr=ricetta.ottieni_attributi(attr_lunghi);
                    for(int i=0;i<attr.length;i++) {
                        if(!(ritorno.contains(attr[i]))) {
                            ritorno.add(attr[i]);
                        }
                    }
                    cursor.moveToNext();
                }
            }
        }
        cursor.close();
        return ritorno;
    }
    //cosa=0 ingredienti; cosa=1 attributi
    public void gestisci_listView(View v,final TextView t,int cosa) {
        if(cosa==0) {
            ArrayList<String> Ing = ottieni_ingredienti_disponibili();
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, Ing);
            final ListView listView = (ListView) v.findViewById(R.id.listview_ingredienti);
            listView.setVisibility(View.VISIBLE);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object o = parent.getItemAtPosition(position);
                    t.setText(o.toString());
                    listView.setVisibility(View.GONE);
                }
            });
        }
        else {
            ArrayList<String> Attr=ottieni_attributi_disponibili();
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, Attr);
            final ListView listView=(ListView) v.findViewById(R.id.listview_attributi);
            listView.setVisibility(View.VISIBLE);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object o = parent.getItemAtPosition(position);
                    t.setText(o.toString());
                    listView.setVisibility(View.GONE);
                }
            });
        }
    }

}
