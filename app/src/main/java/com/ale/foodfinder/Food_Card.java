package com.ale.foodfinder;

/**
 * Created by ale on 12/06/16.
 */
public class Food_Card {
    public Food_Card(String imageid,String name, String type,String difficolta) {
        this.setImageid(imageid);
        this.setName(name);
        this.setType(type);
        this.setDifficolta(difficolta);
    }
    private String name,type,difficolta,imageid;

    public String getImageid() {
        return imageid;
    }

    public void setImageid(String imageid) {
        this.imageid = imageid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDifficolta() {
        return difficolta;
    }

    public void setDifficolta(String difficolta) {
        this.difficolta = difficolta;
    }
}
